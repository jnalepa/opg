# Segmenting pediatric optic pathway gliomas from MRI using deep learning

**J. Nalepa, S. Adamski, K. Kotowski, S. Chelstowska, M. Machnikowska-Sokolowska, Oskar Bozek, Agata Wisz, and Elzbieta Jurkiewicz**

This repository contains supplementary material to the manuscript submitted to Computers in Biology and Medicine:

- **deep architectures/** &mdash; a directory containing the final architectures elaborated for the BraTS and WAW datasets (note that the BraTS architectures may exploit the PT/TL training strategies, as discussed in the manuscript),
- **Training curves** &mdash; training curves of the investigated deep models,
- **Hyperparameters and preprocessing** &mdash; hyperparameters of the models, including the data augmentation hyperparameter values.


